#+TITLE: The Semantic Social Network
#+AUTHOR: pukkamustard
#+OPTIONS: timestamp:nil
#+OPTIONS: reveal_single_file:t
#+REVEAL_ROOT: file:///home/aa/dev/reveal.js-3.8.0/
#+REVEAL_EXTRA_CSS: style.css
#+REVEAL_HLEVEL: 1
#+REVEAL_THEME: white
#+REVEAL_TRANS: none
#+OPTIONS: toc:nil
#+OPTIONS: num:nil

#+BEGIN_SRC emacs-lisp :exports none
(setq org-confirm-babel-evaluate nil)
#+END_SRC

#+RESULTS:
* The Semantic Social Network
  
What is that?

#+ATTR_REVEAL: :frag t
What does it have to do with ActivityPub?

** Overview

- Semantic Data/Linked Data
  - Example: Touristic data
- ActivityPub and Linked Data
- The Fediverse

* Semantic Data/Linked Data
** Engadin
[[./images/engadin.jpg]]

** Touristic Data
- Hospitality & Accommodation
- Gastronomy
- Events
  - Venues
- Points of Interest
- Activities
  - Hiking
  - Winter sports
- Real time data
  - Weather
  - Road/trail status

#+BEGIN_NOTES
- How is this data used:
  - Portals for prospective visitors
  - Show related information for tourists who have already booked accomodation/snow pass
#+END_NOTES

** Touristic Data

#+BEGIN_SRC dot :file images/td-model1.png
graph {
  graph [   rankdir = "LR" ];
  ranksep=2;

  "Accomodation" [ shape="record" ];
  "Gastronomy" [ shape="record" ];
  "Event" [ shape="record" ];
  "Venue" [ shape="record" ];

  Accomodation--Address;
  Gastronomy--Address;
  
  Venue--Accomodation;
  Venue--Gastronomy;

  Event--Venue;

  "Point of Interest" [ shape="record" ];

  "Point of Interest"--Address;
  Event--"Point of Interest";

  "Hiking trail" [ shape="record" ];
  "Ski region" [ shape="record" ];

  "Point of Interest"--"Hiking trail";
  "Hiking trail"--"Gastronomy";
  "Ski region"--"Gastronomy";
  "Ski region"--"Accomodation";

  "Road condition" [ shape="record" ];
  "Road condition"--"Accomodation";

  "Trail status" [ shape="record" ];
  "Hiking trail"--"Trail status";

}
#+END_SRC

#+RESULTS:
[[file:images/td-model1.png]]


- A lot of different kinds of data and a lot of links
- Relational model:
  - Slightly complicated
  - Not very flexible
  - How to query?
 
** A simpler model: a graph

#+BEGIN_SRC dot :file images/graph1.png
digraph {
  graph [   rankdir = "LR" ];
  "Restaurant Schweizerhof"->"Restaurant" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/graph1.png]]

** 
   
#+BEGIN_SRC dot :file images/graph2.png
digraph {
  graph [   rankdir = "LR" ];
  "Restaurant Schweizerhof"->"restaurant" [label="is a"];
  "Restaurant Schweizerhof"->"Gourmet's Hike" [label="lies on"];
}
#+END_SRC

#+RESULTS:
[[file:images/graph2.png]]

** 

#+BEGIN_SRC dot :file images/graph3.png
digraph {
  graph [   rankdir = "LR" ];
  "Restaurant Schweizerhof"->"restaurant" [label="is a"];
  "Restaurant Schweizerhof"->"Gourmet's Hike" [label="lies on"];

  "Johnny Cash Live"->"Restaurant Schweizerhof" [label="venue"];
  "Johnny Cash Live"->"concert" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/graph3.png]]

#+ATTR_REVEAL: :frag t
"Show me all concerts that I could attend while hiking the Gourmet's hike."

** Triple

#+BEGIN_SRC dot :file images/triple1.png
digraph {
  graph [   rankdir = "LR" ];
  "subject"->"object" [label="predicate"];
}
#+END_SRC

#+RESULTS:
[[file:images/triple1.png]]

** Triple

#+BEGIN_SRC dot :file images/triple2.png
digraph {
  graph [   rankdir = "LR" ];
  "subject"->"object 1" [label="predicate 1"];
  "subject"->"object 2" [label="predicate 2"];
  "subject"->"object 3" [label="predicate 3"];
}
#+END_SRC

#+RESULTS:
[[file:images/triple2.png]]

Subjects can have multiple outgoing edges.

** Triple
#+BEGIN_SRC dot :file images/triple3.png
digraph {
  graph [   rankdir = "LR" ];
  "subject"->"object 1" [label="predicate 1"];
  "subject"->"object 2" [label="predicate 2"];
  "subject"->"object 3" [label="predicate 3"];
  "another subject"->"subject" [label="predicate 4"];
}
#+END_SRC

#+RESULTS:
[[file:images/triple3.png]]

Subjects can be object of another subject.
** Naming
#+BEGIN_SRC dot :file images/naming1.png
digraph {
  graph [   rankdir = "LR" ];
  "Johnny Cash Live"->"Restaurant Schweizerhof" [label="venue"];
  "Johnny Cash Live"->"concert" [label="is a"];

  "Johnny Cash Concert"->"Restaurant Schweizerhof" [label="venue"];
  "Johnny Cash Concert"->"concert" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/naming1.png]]

"Johnny Cash Live" and "Johnny Cash Concert" refer to the same concert.

#+ATTR_REVEAL: :frag t
We need a naming convention.

** Naming
#+BEGIN_SRC dot :file images/naming2.png
digraph {
  graph [   rankdir = "LR" ];
  JohnnyCashConcert [label="http://engadin.ch/concerts/johnny-cash"];
  Schweizerhof [label="http://restaurant-schweizerhof.ch/"];

  JohnnyCashConcert->Schweizerhof [label="venue"];
  JohnnyCashConcert->"concert" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/naming2.png]]

Use URIs to name things.

** Naming
#+BEGIN_SRC dot :file images/naming3.png
digraph {
  graph [   rankdir = "LR" ];
  JohnnyCashConcert [label="http://engadin.ch/concerts/johnny-cash"];
  Schweizerhof [label="http://restaurant-schweizerhof.ch/"];

  JohnnyCashConcert->"\"Johnny Cash Live\"" [label="name"];
  Schweizerhof->"\"Restaurant Schweizerhof\"" [label="name"];

  JohnnyCashConcert->Schweizerhof [label="venue"];
  JohnnyCashConcert->"concert" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/naming3.png]]

Use the "name" property for human readable name.

** Naming
#+BEGIN_SRC dot :file images/naming4.png
digraph {
  graph [   rankdir = "LR" ];
  JohnnyCashConcert [label="http://engadin.ch/concerts/johnny-cash"];
  Schweizerhof [label="http://restaurant-schweizerhof.ch/"];

  JohnnyCashConcert->"\"Johnny Cash Live\"" [label="name"];
  Schweizerhof->"\"Restaurant Schweizerhof\"" [label="name"];

  JohnnyCashConcert->Schweizerhof [label="venue"];
  JohnnyCashConcert->"http://schema.org/MusicEvent" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/naming4.png]]

Also use URIs to name abstract things.

A lot of things already have a URI to name them (see http://schema.org).

** Naming
#+BEGIN_SRC dot :file images/naming5.png
digraph {
  graph [   rankdir = "LR" ];
  JohnnyCashConcert [label="http://engadin.ch/concerts/johnny-cash"];
  Schweizerhof [label="http://restaurant-schweizerhof.ch/"];

  JohnnyCashConcert->"\"Johnny Cash Live\"" [label="http://schema.org/name"];
  Schweizerhof->"\"Restaurant Schweizerhof\"" [label="http://schema.org/name"];

  JohnnyCashConcert->Schweizerhof [label="http://schema.org/location"];
  JohnnyCashConcert->"http://schema.org/MusicEvent" [label="http://www.w3.org/1999/02/22-rdf-syntax-ns#type"];
}
#+END_SRC

#+RESULTS:
[[file:images/naming5.png]]

Also use URIs to name properties.

** Our data model
#+BEGIN_SRC dot :file images/triple4.png
digraph {
  graph [   rankdir = "LR" ];
  "http://example.org/subject"->"http://example.org/object" [label="http://example.org/predicate"];
}
#+END_SRC

#+RESULTS:
[[file:images/triple4.png]]

- Basic unit is a triple consisting of a subject, predicate and object
- Name (or identify) everything with URIs
 
#+ATTR_REVEAL: :frag t
This is (almost) the Resource Description Framework (RDF) data model. 

** Remote content

#+BEGIN_SRC dot :file images/remote-data.png
digraph {
  graph [   rankdir = "LR" ];

  

  "http://example.org/subject"->"http://example.org/object" [label="http://example.org/predicate"];

  "http://example.org/subject"->"http://somewhere-else.org/something" [label="http://example.org/predicate2"]

  subgraph cluster_followup {
    label="Remote content";
    "http://somewhere-else.org/something"->"http://somewhere-else.org/something-else" [label="http://example.org/blups"];
  }
}
#+END_SRC

#+RESULTS:
[[file:images/remote-data.png]]

- The names (URIs) can be dereferenced and provide more information.
- Remote content can be referenced

#+ATTR_REVEAL: :frag t
Linked Data!

** Back to Engadin

#+BEGIN_SRC dot :file images/graph4.png
digraph {
  graph [   rankdir = "LR" ];
  "Restaurant Schweizerhof"->"restaurant" [label="is a"];
  "Restaurant Schweizerhof"->"Gourmet's Hike" [label="lies on"];

  "Johnny Cash Live"->"Restaurant Schweizerhof" [label="venue"];
  "Johnny Cash Live"->"concert" [label="is a"];
}
#+END_SRC

#+RESULTS:
[[file:images/graph4.png]]

- Flexible data model
- Meaningful queries (semantic queries)
- Can reference remote content
 
#+ATTR_REVEAL: :frag t
Data needs to be crowdsourced.
#+ATTR_REVEAL: :frag t
ActivityPub!

* ActivityPub and Linked Data
** Alyssa posts a note

[[./images/tutorial-3.png]]

#+BEGIN_SRC js
{
 "@context": "https://www.w3.org/ns/activitystreams",
 "type": "Create",
 "to": ["https://chatty.example/ben/"],
 "actor": "https://social.example/alyssa/",
 "object": {"type": "Note",
            "attributedTo": "https://social.example/alyssa/",
            "to": ["https://chatty.example/ben/"],
            "content": "Say, did you finish reading that book I lent you?"}
}
#+END_SRC

** Alyssa posts a graph

#+BEGIN_SRC js
{"@context": "https://www.w3.org/ns/activitystreams",
 "type": "Create",
 "to": ["https://chatty.example/ben/"],
 "actor": "https://social.example/alyssa/",
 "object": {"type": "Note",
            "attributedTo": "https://social.example/alyssa/",
            "to": ["https://chatty.example/ben/"],
            "content": "Say, did you finish reading that book I lent you?"}}
#+END_SRC

#+BEGIN_SRC dot :file images/alyssas-note.png
digraph {
  graph [   rankdir = "LR" ];
  
  Activity [label="_"];

  Activity->"Create" [label="type"];
  Activity->"https://chatty.example/ben" [label="to"];
  Activity->"https://social.example/alyssa" [label="actor"];

  Object [label="_"];

  Activity->Object [label="object"];

  Object->"Note" [label="type"];
  Object->"https://social.example/alyssa" [label="attributedTo"];
  Object->"https://chatty.example/ben" [label="to"];
  Object->"\"Say, did you finish reading that book I lent you?\"" [label="content"];
}
#+END_SRC

#+RESULTS:
[[file:images/alyssas-note.png]]

** Alyssa posts a graph
[[file:images/alyssas-note.png]]

- No names/ids for Create activity or object (yet)
#+ATTR_REVEAL: :frag t
- A bit like RDF...URIs missing

** The @context field

#+BEGIN_SRC js
 "@context": "https://www.w3.org/ns/activitystreams"
#+END_SRC

#+ATTR_REVEAL: :frag t
#+BEGIN_SRC
curl -LH "Accept: application/ld+json" https://www.w3.org/ns/activitystreams
#+END_SRC

#+ATTR_REVEAL: :frag t
#+BEGIN_SRC js
{
  "@context": {
    "@vocab": "_:",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "as": "https://www.w3.org/ns/activitystreams#",
    "ldp": "http://www.w3.org/ns/ldp#",
    "id": "@id",
    "type": "@type",
    "Accept": "as:Accept",
    "Activity": "as:Activity",
    "IntransitiveActivity": "as:IntransitiveActivity",
    "Add": "as:Add",
    "Announce": "as:Announce",
    "Application": "as:Application",
    "Arrive": "as:Arrive",
    "Article": "as:Article",
    "Audio": "as:Audio",
    "Block": "as:Block",
    "Collection": "as:Collection",
    "CollectionPage": "as:CollectionPage",
    "Relationship": "as:Relationship",
    "Create": "as:Create",
    "Delete": "as:Delete",
    "Dislike": "as:Dislike",
    "Document": "as:Document",
    "Event": "as:Event",
    "Follow": "as:Follow",
    "Flag": "as:Flag",
    "Group": "as:Group",
    "Ignore": "as:Ignore",
    "Image": "as:Image",
    "Invite": "as:Invite",
    "Join": "as:Join",
    "Leave": "as:Leave",
    "Like": "as:Like",
    "Link": "as:Link",
    "Mention": "as:Mention",
    "Note": "as:Note",
    "Object": "as:Object",
    "Offer": "as:Offer",
    "OrderedCollection": "as:OrderedCollection",
    "OrderedCollectionPage": "as:OrderedCollectionPage",
    "Organization": "as:Organization",
    "Page": "as:Page",
    "Person": "as:Person",
    "Place": "as:Place",
    "Profile": "as:Profile",
    "Question": "as:Question",
    "Reject": "as:Reject",
    "Remove": "as:Remove",
    "Service": "as:Service",
    "TentativeAccept": "as:TentativeAccept",
    "TentativeReject": "as:TentativeReject",
    "Tombstone": "as:Tombstone",
    "Undo": "as:Undo",
    "Update": "as:Update",
    "Video": "as:Video",
    "View": "as:View",
    "Listen": "as:Listen",
    "Read": "as:Read",
    "Move": "as:Move",
    "Travel": "as:Travel",
    "IsFollowing": "as:IsFollowing",
    "IsFollowedBy": "as:IsFollowedBy",
    "IsContact": "as:IsContact",
    "IsMember": "as:IsMember",
    "subject": {
      "@id": "as:subject",
      "@type": "@id"
    },
    "relationship": {
      "@id": "as:relationship",
      "@type": "@id"
    },
    "actor": {
      "@id": "as:actor",
      "@type": "@id"
    },
    "attributedTo": {
      "@id": "as:attributedTo",
      "@type": "@id"
    },
    "attachment": {
      "@id": "as:attachment",
      "@type": "@id"
    },
    "bcc": {
      "@id": "as:bcc",
      "@type": "@id"
    },
    "bto": {
      "@id": "as:bto",
      "@type": "@id"
    },
    "cc": {
      "@id": "as:cc",
      "@type": "@id"
    },
    "context": {
      "@id": "as:context",
      "@type": "@id"
    },
    "current": {
      "@id": "as:current",
      "@type": "@id"
    },
    "first": {
      "@id": "as:first",
      "@type": "@id"
    },
    "generator": {
      "@id": "as:generator",
      "@type": "@id"
    },
    "icon": {
      "@id": "as:icon",
      "@type": "@id"
    },
    "image": {
      "@id": "as:image",
      "@type": "@id"
    },
    "inReplyTo": {
      "@id": "as:inReplyTo",
      "@type": "@id"
    },
    "items": {
      "@id": "as:items",
      "@type": "@id"
    },
    "instrument": {
      "@id": "as:instrument",
      "@type": "@id"
    },
    "orderedItems": {
      "@id": "as:items",
      "@type": "@id",
      "@container": "@list"
    },
    "last": {
      "@id": "as:last",
      "@type": "@id"
    },
    "location": {
      "@id": "as:location",
      "@type": "@id"
    },
    "next": {
      "@id": "as:next",
      "@type": "@id"
    },
    "object": {
      "@id": "as:object",
      "@type": "@id"
    },
    "oneOf": {
      "@id": "as:oneOf",
      "@type": "@id"
    },
    "anyOf": {
      "@id": "as:anyOf",
      "@type": "@id"
    },
    "closed": {
      "@id": "as:closed",
      "@type": "xsd:dateTime"
    },
    "origin": {
      "@id": "as:origin",
      "@type": "@id"
    },
    "accuracy": {
      "@id": "as:accuracy",
      "@type": "xsd:float"
    },
    "prev": {
      "@id": "as:prev",
      "@type": "@id"
    },
    "preview": {
      "@id": "as:preview",
      "@type": "@id"
    },
    "replies": {
      "@id": "as:replies",
      "@type": "@id"
    },
    "result": {
      "@id": "as:result",
      "@type": "@id"
    },
    "audience": {
      "@id": "as:audience",
      "@type": "@id"
    },
    "partOf": {
      "@id": "as:partOf",
      "@type": "@id"
    },
    "tag": {
      "@id": "as:tag",
      "@type": "@id"
    },
    "target": {
      "@id": "as:target",
      "@type": "@id"
    },
    "to": {
      "@id": "as:to",
      "@type": "@id"
    },
    "url": {
      "@id": "as:url",
      "@type": "@id"
    },
    "altitude": {
      "@id": "as:altitude",
      "@type": "xsd:float"
    },
    "content": "as:content",
    "contentMap": {
      "@id": "as:content",
      "@container": "@language"
    },
    "name": "as:name",
    "nameMap": {
      "@id": "as:name",
      "@container": "@language"
    },
    "duration": {
      "@id": "as:duration",
      "@type": "xsd:duration"
    },
    "endTime": {
      "@id": "as:endTime",
      "@type": "xsd:dateTime"
    },
    "height": {
      "@id": "as:height",
      "@type": "xsd:nonNegativeInteger"
    },
    "href": {
      "@id": "as:href",
      "@type": "@id"
    },
    "hreflang": "as:hreflang",
    "latitude": {
      "@id": "as:latitude",
      "@type": "xsd:float"
    },
    "longitude": {
      "@id": "as:longitude",
      "@type": "xsd:float"
    },
    "mediaType": "as:mediaType",
    "published": {
      "@id": "as:published",
      "@type": "xsd:dateTime"
    },
    "radius": {
      "@id": "as:radius",
      "@type": "xsd:float"
    },
    "rel": "as:rel",
    "startIndex": {
      "@id": "as:startIndex",
      "@type": "xsd:nonNegativeInteger"
    },
    "startTime": {
      "@id": "as:startTime",
      "@type": "xsd:dateTime"
    },
    "summary": "as:summary",
    "summaryMap": {
      "@id": "as:summary",
      "@container": "@language"
    },
    "totalItems": {
      "@id": "as:totalItems",
      "@type": "xsd:nonNegativeInteger"
    },
    "units": "as:units",
    "updated": {
      "@id": "as:updated",
      "@type": "xsd:dateTime"
    },
    "width": {
      "@id": "as:width",
      "@type": "xsd:nonNegativeInteger"
    },
    "describes": {
      "@id": "as:describes",
      "@type": "@id"
    },
    "formerType": {
      "@id": "as:formerType",
      "@type": "@id"
    },
    "deleted": {
      "@id": "as:deleted",
      "@type": "xsd:dateTime"
    },
    "inbox": {
      "@id": "ldp:inbox",
      "@type": "@id"
    },
    "outbox": {
      "@id": "as:outbox",
      "@type": "@id"
    },
    "following": {
      "@id": "as:following",
      "@type": "@id"
    },
    "followers": {
      "@id": "as:followers",
      "@type": "@id"
    },
    "streams": {
      "@id": "as:streams",
      "@type": "@id"
    },
    "preferredUsername": "as:preferredUsername",
    "endpoints": {
      "@id": "as:endpoints",
      "@type": "@id"
    },
    "uploadMedia": {
      "@id": "as:uploadMedia",
      "@type": "@id"
    },
    "proxyUrl": {
      "@id": "as:proxyUrl",
      "@type": "@id"
    },
    "liked": {
      "@id": "as:liked",
      "@type": "@id"
    },
    "oauthAuthorizationEndpoint": {
      "@id": "as:oauthAuthorizationEndpoint",
      "@type": "@id"
    },
    "oauthTokenEndpoint": {
      "@id": "as:oauthTokenEndpoint",
      "@type": "@id"
    },
    "provideClientKey": {
      "@id": "as:provideClientKey",
      "@type": "@id"
    },
    "signClientKey": {
      "@id": "as:signClientKey",
      "@type": "@id"
    },
    "sharedInbox": {
      "@id": "as:sharedInbox",
      "@type": "@id"
    },
    "Public": {
      "@id": "as:Public",
      "@type": "@id"
    },
    "source": "as:source",
    "likes": {
      "@id": "as:likes",
      "@type": "@id"
    },
    "shares": {
      "@id": "as:shares",
      "@type": "@id"
    }
  }
}
#+END_SRC

** Context gives us names
#+BEGIN_SRC js
{
  "@context": {
    "as": "https://www.w3.org/ns/activitystreams#",
    "Create": "as:Create",
}
#+END_SRC

~Create~ → ~https://www.w3.org/ns/activitystreams#Create~

** Alyssa posts Linked Data

#+BEGIN_SRC dot :file images/alyssas-note2.png
digraph {
  graph [   rankdir = "LR" ];
  
  Activity [label="_"];

  Activity->"https://www.w3.org/ns/activitystreams#Create" [label="https://www.w3.org/ns/activitystreams#type"];
  Activity->"https://chatty.example/ben" [label="https://www.w3.org/ns/activitystreams#to"];
  Activity->"https://social.example/alyssa" [label="https://www.w3.org/ns/activitystreams#actor"];

  Object [label="_"];

  Activity->Object [label="https://www.w3.org/ns/activitystreams#object"];

  Object->"https://www.w3.org/ns/activitystreams#Note" [label="https://www.w3.org/ns/activitystreams#type"];
  Object->"https://social.example/alyssa" [label="https://www.w3.org/ns/activitystreams#attributedTo"];
  Object->"https://chatty.example/ben" [label="https://www.w3.org/ns/activitystreams#to"];
  Object->"\"Say, did you finish reading that book I lent you?\"" [label="https://www.w3.org/ns/activitystreams#content"];
}
#+END_SRC

#+RESULTS:
[[file:images/alyssas-note2.png]]


#+ATTR_REVEAL: :frag t
This is Linked Data!

#+ATTR_REVEAL: :frag t
JSON-LD is a serialization of Linked Data.

** Alyssa posts Linked Data

#+BEGIN_SRC dot :file images/alyssas-note3.png
digraph {
  graph [   rankdir = "LR" ];
  
  Activity [label="_"];

  Activity->"as:Create" [label="as:type"];
  Activity->"https://chatty.example/ben" [label="as:to"];
  Activity->Alyssa [label="as:actor"];

  Object [label="_"];

  Activity->Object [label="as:object"];

  Object->"as:Note" [label="as:type"];
  Object->Alyssa [label="as:attributedTo"];
  Object->"https://chatty.example/ben" [label="as:to"];
  Object->"\"Say, did you finish reading that book I lent you?\"" [label="as:content"];


  subgraph cluster_followup {
    Alyssa [label="https://social.example/alyssa"];
    Alyssa->"as:Person" [label="as:type"];
    Alyssa->"\"Alyssa P. Hacker\"" [label="as:name"];
    Alyssa->"https://social.example/alyssa/inbox/" [label="as:inbox"];
    Alyssa->"https://social.example/alyssa/followers/" [label="as:followers"];
  }
}
#+END_SRC

#+RESULTS:
[[file:images/alyssas-note3.png]]

** Linking to other ActivityPub activities

#+BEGIN_SRC dot :file images/alyssas-like.png
digraph {
  graph [   rankdir = "LR" ];
  
  subgraph cluster__alyssas_post {
    Activity [label="https://social.example/alyssa/posts/1"];

    Activity->"as:Create" [label="as:type"];
    Activity->"https://chatty.example/ben" [label="as:to"];
    Activity->Alyssa [label="as:actor"];

    Object [label="_"];

    Activity->Object [label="as:object"];

    Object->"as:Note" [label="as:type"];
    Object->"https://social.example/alyssa" [label="as:attributedTo"];
    Object->"https://chatty.example/ben" [label="as:to"];
    Object->"\"Say, did you finish reading that book I lent you?\"" [label="as:content"];
  }

  subgraph cluster__bens_like {
    Like [label="http://chatty.example.com/ben/posts/2"];
    Like->Activity [label="as:object"];
    Like->"as:Like" [label="as:type"];
    Like->"http://chatty.example/ben" [label="as:actor"];
  }

}
#+END_SRC

#+RESULTS:
[[file:images/alyssas-like.png]]

** Linking to anything
#+BEGIN_SRC dot :file images/alyssas-link-book.png
digraph {
  graph [   rankdir = "LR" ];
  
  subgraph cluster__book{
    Book [label="https://books.com/a-book"];
    Book->"http://schema.org/Book" [label="type"];
    Book->"\"A Book\"" [label="name"];
  }

  subgraph cluster__alyssas_post {
    Activity [label="_"];

    Activity->"as:Create" [label="as:type"];
    Activity->"https://chatty.example/ben" [label="as:to"];
    Activity->Alyssa [label="as:actor"];

    Object [label="_"];

    Activity->Object [label="as:object"];

    Object->"as:Note" [label="as:type"];
    Object->"https://social.example/alyssa" [label="as:attributedTo"];
    Object->"https://chatty.example/ben" [label="as:to"];
    Object->"\"Say, did you finish reading that book I lent you?\"" [label="as:content"];
    Object->Book [label="refersTo"];
  }

}
#+END_SRC

#+RESULTS:
[[file:images/alyssas-link-book.png]]

Any remote content that is in some structured format (RDFa, JSON-LD, ...) can be referenced.

#+ATTR_REVEAL: :frag t
Federation beyond ActivityPub.

** Linked Data and semantic queries
#+BEGIN_SRC dot :file images/alyssas-link-book2.png
digraph {
  graph [   rankdir = "LR" ];
  
  subgraph cluster__book{
    Book [label="https://books.com/a-book"];
    Book->"http://schema.org/Book" [label="type"];
    Book->"\"A Book\"" [label="name"];
  }

  subgraph cluster__alyssas_post {
    Activity [label="_"];

    Activity->"as:Create" [label="as:type"];
    Activity->"https://chatty.example/ben" [label="as:to"];
    Activity->Alyssa [label="as:actor"];

    Object [label="_"];

    Activity->Object [label="as:object"];

    Object->"as:Note" [label="as:type"];
    Object->"https://social.example/alyssa" [label="as:attributedTo"];
    Object->"https://chatty.example/ben" [label="as:to"];
    Object->"\"Say, did you finish reading that book I lent you?\"" [label="as:content"];
    Object->Book [label="refersTo"];
  }

  subgraph cluster__something_else {
    "Something else"->Book [label="refersTo"];
  }

  subgraph cluster__someone_else {
    "Someone else"->Book [label="refersTo"];
  }
}
#+END_SRC

#+RESULTS:
[[file:images/alyssas-link-book2.png]]

#+ATTR_REVEAL: :frag t
Who all is talking about that book?

** ActivityPub content is highly interlinked
   
- Commenting on something
- Liking something
- Following someone
 
#+ATTR_REVEAL: :frag t
Content is often remote (on a federated server).

* The Fediverse
[[./images/fediverse.png]]

#+ATTR_REVEAL: :frag t
A distributed graph of interlinked content created by social interactions.

#+ATTR_REVEAL: :frag t
The Semantic Social Network

** So what?

#+ATTR_REVEAL: :frag t
- ActivityPub content can be seen as documents or as graphs
#+ATTR_REVEAL: :frag t
- Graph can be traversed and queried in interesting ways
#+ATTR_REVEAL: :frag t
- There is no limit to the kind of data that can be created in a crowdsourced manner
#+ATTR_REVEAL: :frag t
- Open data sets are publicly available → link to them ([[https://www.w3.org/DesignIssues/LinkedData.html][5-star open data]])
#+ATTR_REVEAL: :frag t
- The Semantic Web community (tools, research & standards)

* Acknowledgments

[[./images/mia-engiadina.png]]

** openEngiadina

Platform for creating, publishing and using open local knowledge.

https://miaengiadina.github.io/openengiadina/

* [[./images/thanks.png]]

pukkamustard@posteo.net

https://inqlab.net

* Extra
** Links

- [[https://json-ld.org/playground/][JSON-LD Playground]]: Interactively expand a JSON-LD document into a graph.
- [[https://www.w3.org/TR/rdf11-primer/][RDF 1.1. Primer]]
- [[https://www.w3.org/DesignIssues/LinkedData.html][Tim Berners-Lee's explanation of Linked Data]]
- Vocabularies
  - [[https://schema.org/][schema.org]]: Vocabulary for structured data
  - [[https://www.dublincore.org/][Dublin Core Metadata Initiative]]
  - [[http://www.foaf-project.org/][Friend of a Friend (FOAF)]]
- [[https://www.downes.ca/cgi-bin/page.cgi?post=46][The Semantic Social Network (2004)]]: Article by Stephen Dowes
    
